const login = () => {

    const email = $('#email').val(),
          password = $('#password').val(),
          loginButton = $('#login-button')

    
    loginButton.html(
        `<div class="spinner-border text-light" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>`
    )

    loginButton.prop('disabled', true)

    $.ajax({
        method: 'POST',
        url: 'http://34.222.146.56:8000/auth/login-users/',
        data: JSON.stringify({email: email, password: password}),
        headers: {
            'Content-Type': 'application/json'
        },
        success: (res) => {

            localStorage.setItem('token', res.token)
            localStorage.setItem('role_id', res.role_id)
            localStorage.setItem('user_email', email)
            localStorage.setItem('user_id', res.userId)

            $('.auth-message').html(

                `<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Welcome back!
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>`
            )
            
            setTimeout(() => {
                location.href = './'
            }, 2000);
        },
        statusCode: {
            401: () => {
                $('.auth-message').html(

                    `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                        Wrong email or password, try again.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`
                )
                loginButton.prop('disabled', false)
                loginButton.html(
                    `Login`
                )
            },
            500: () => {
                $('.auth-message').html(

                    `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                        We have a problem processing your request, try again later.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`
                )
                loginButton.prop('disabled', false)
                loginButton.html(
                    `Login`
                )
            }
        }
    })
}

const register = () => {

    const 
        name = $('#name').val(),
        email = $('#email').val(),
        password = $('#password').val(),
        registerButton = $('#register-button')

    
    registerButton.html(
        `<div class="spinner-border text-light" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>`
    )

    registerButton.prop('disabled', true)

    $.ajax({
        method: 'POST',
        url: 'http://34.222.146.56:8000/auth/register-users/',
        data: JSON.stringify({name:name, email: email, password: password}),
        headers: {
            'Content-Type': 'application/json'
        },
        success: (res) => {

            localStorage.setItem('token', res.token)
            localStorage.setItem('role_id', res.role_id)
            localStorage.setItem('user_email', email)
            localStorage.setItem('user_id', res.userId)

            $('.auth-message').html(

                `<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Welcome!
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>`
            )
            
            setTimeout(() => {
                location.href = './'
            }, 2000);
        },
        statusCode: {
            401: () => {
                $('.auth-message').html(

                    `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                        User already exists.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`
                )
                registerButton.prop('disabled', false)
                registerButton.html(
                    `Register`
                )
            },
            500: () => {
                $('.auth-message').html(

                    `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                        We have a problem processing your request, try again later.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`
                )
                registerButton.prop('disabled', false)
                registerButton.html(
                    `Register`
                )
            }
        }
    })
}


const logout = () => {
    localStorage.clear()
    location.href="./pages-login.php"
}