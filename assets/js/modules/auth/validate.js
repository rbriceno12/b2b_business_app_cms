const validateToken = () => {

    $.ajax({
        method: 'GET',
        url: `http://34.222.146.56:8000/auth/verify/`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {
            return 'Logged'
        },
        statusCode: {
            403: () => {
                location.href="./pages-login.php?sessionExpired=true"
            },
            500: () => {
                location.href="./pages-login.php?sessionExpired=true"
            }
        }
    })
}

validateToken()