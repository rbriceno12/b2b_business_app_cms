const role = localStorage.getItem('role_id')

const listCategories = (created) => {


    $.ajax({
        method: 'GET',
        url: 'http://34.222.146.56:8000/categories/list/cms',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {

            let listBody = ``,
                showButtons,
                greenBackground,
                counter = 0,
                status = ``,
                showActivateDependingOnStatus = ``
                showDesactivateDependingOnStatus = ``

            role == 1  || role == 2 ? showButtons = '' : showButtons = 'display:none;'
            
            res.data.forEach(category => {

                counter++
                if(category.status == 'activate'){
                    status = 'Activa'
                    showActivateDependingOnStatus = 'display:none;'
                    showDesactivateDependingOnStatus = ''
                }else{
                    status = 'Inactiva'
                    showActivateDependingOnStatus = ''
                    showDesactivateDependingOnStatus = 'display:none;'
                }


                counter == 1 && created ? greenBackground = 'background-color: #09ab1da6; color: white;' : greenBackground = ''

                listBody+= `
                    <tr>
                        <td style="${greenBackground}">${category.id}</td>
                        <td style="${greenBackground}"><img style="width:10%;" src="http://34.222.146.56:8000/public/uploads/${category.img}" target="_blank"></a></img></td>
                        <td style="${greenBackground}">${category.name}</td>
                        <td style="${greenBackground}">${status}</td>
                        <td style="${greenBackground}">${category.created_at}</td>
                        <td style="${greenBackground}">
                            <button style="${showButtons} ${showActivateDependingOnStatus}" onclick="activateCategory(${category.id})" type="button" class="btn btn-success"><i class="bi bi-check-circle"></i></button>
                            <button style="${showButtons} ${showDesactivateDependingOnStatus}" onclick="desactivateCategory(${category.id})" type="button" class="btn btn-danger"><i class="bi bi-exclamation-octagon"></i></button>
                        </td>
                    </tr>`
                
            });

            $('.categories-table-body').html(
                listBody
            )
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}

listCategories(false)


const activateCategory = (id) => {
        $.ajax({
            method: 'PATCH',
            url: 'http://34.222.146.56:8000/categories/activate',
            data: JSON.stringify({id:id}),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            success: (res) => {
                listCategories(false)
            },
            statusCode: {
                401: () => {
                    console.log('error')
                },
                500: () => {
                    console.log('error')
                }
            }
        })
    
}

const desactivateCategory = (id) => {
    
    $.ajax({
        method: 'PATCH',
        url: 'http://34.222.146.56:8000/categories/desactivate',
        data: JSON.stringify({id:id}),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {
            listCategories(false)
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })

}


$('#createCategoryForm').submit(function(e) {
    e.preventDefault(); // Prevent default form submission

    const name                = $('#name').val(),
          file                = $('#file').val(),
          createCategoryButton = $('#create-category-button')

    console.log('Llegamos')
        
    if(!name || !file){
        $('.create-category-message').html(

            `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                Missing required fields
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`
        )
    }else{
        createCategoryButton.html(
            `<div class="spinner-border text-light" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>`
        )

        createCategoryButton.prop('disabled', true)

        const formData = new FormData(this); // Create FormData object

        $.ajax({
            url: 'http://34.222.146.56:8000/upload-category', // URL of your Node.js route handler
            type: 'POST',
            data: formData, // Attach FormData object to request
            contentType: false, // Set content type to false for multipart/form-data
            processData: false, // Prevent jQuery from processing data
            success: (data) => {

                $('#createCategoryModal').modal('hide')
                listCategories(true)
                createCategoryButton.prop('disabled', false)
                createCategoryButton.html(
                    `Save changes`
                )
                $('.create-category-message').fadeOut('fast')
                $('#name').val('')
                $('#file').val('')
                // Handle successful upload response
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.error('Upload failed:', textStatus, errorThrown);
                // Handle upload error
            }
        });
    }

});

