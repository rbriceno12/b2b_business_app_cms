const role = localStorage.getItem('role_id')

const showDashboardData = () => {


        $.ajax({
            method: 'GET',
            url: 'http://34.222.146.56:8000/dashboard/data/',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            success: (res) => {
                console.log(res.data)
    
                $('#count-sales').html(res.data.countInvoices)
                $('#total-sales').html(`$${res.data.totalInvoices}`)
                $('#count-purchase-orders').html(res.data.countPurchaseOrders)
                $('#count-clients').html(res.data.totalUsers)
                $('#total-comissions').html(`$${res.data.comissions}`)

                let listInvoicesBody = ``

                let totalValue = 0,
                    totalComission = 0
                
                res.data.last10Invoices.forEach(invoice => {


                    listInvoicesBody+= `<tr>
                                    <td>${invoice.id}</td>
                                    <td>${invoice.email}</td>
                                    <td style="width:20%"> <a target="_blank" href="http://34.222.146.56:8000/public/uploads/${invoice.file}">Ver archivo</a></td>
                                    <td>$${invoice.value}</td>
                                    <td>$${invoice.comission}</td>
                                </tr>`
                    
                    totalValue = totalValue + Number(invoice.value);
                    totalComission = totalComission + Number(invoice.comission);
                    
                });

                listInvoicesBody+= `<tr>
                                        <td colspan="3"></td>
                                        <td>Total: $${(totalValue).toFixed(2)}</td>
                                        <td>Total: $${(totalComission).toFixed(2)}</td>
                                        <td colspan="3"></td>
                                    </tr>`

                $('.last-10-invoices-table').html(
                    listInvoicesBody
                )

                let listPurchaseOrdersBody = ``

                
                res.data.last10PurchaseOrders.forEach(invoice => {


                    listPurchaseOrdersBody+= `<tr>
                                    <td>${invoice.id}</td>
                                    <td>${invoice.email}</td>
                                    <td style="width:20%"> <a target="_blank" href="http://34.222.146.56:8000/public/uploads/${invoice.file}">Ver archivo</a></td>
                                </tr>`
                    
                    totalValue = totalValue + Number(invoice.value);
                    
                });

                $('.last-10-purchase-orders-table').html(
                    listPurchaseOrdersBody
                )
                
            },
            statusCode: {
                401: () => {
                    console.log('error')
                },
                500: () => {
                    console.log('error')
                }
            }
        })
}

showDashboardData()