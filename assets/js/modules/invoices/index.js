const role = localStorage.getItem('role_id')

const listInvoices = () => {


    $.ajax({
        method: 'GET',
        url: 'http://34.222.146.56:8000/invoices/list/',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {

            let listBody = ``
            let showButtons
            

            role == 1  || role == 2 ? showButtons = '' : showButtons = 'display:none;'

            let totalValue = 0,
                totalComission = 0
            
            res.data.forEach(invoice => {


                let invoiceStatus = invoice.status
                let showConfirmButton = ''
                invoiceStatus == 'paid' || invoiceStatus == 'invoice' ? showConfirmButton = 'display:none;' : showConfirmButton = ''
                
                switch (invoiceStatus) {
                    case 'paid':
                        invoiceStatus = 'Pagada'
                        break;
                    case 'cancelled-invoice':
                        invoiceStatus = 'Cancelada'
                        break;
                    case 'invoice':
                        invoiceStatus = 'Pagada'
                        break;
                    default:
                        break;
                }

                listBody+= `<tr>
                                <td>${invoice.id}</td>
                                <td>${invoice.email}</td>
                                <td style="width:20%"> <a target="_blank" href="http://34.222.146.56:8000/public/uploads/${invoice.file}">Ver archivo</a></td>
                                <td>${invoiceStatus}</td>
                                <td>${invoice.created_at}</td>
                                <td>$${invoice.value}</td>
                                <td>$${invoice.comission}</td>
                                <td>
                                    <button style="${showButtons} ${showConfirmButton}" onclick="confirmInvoice(${invoice.id}, '${invoice.email}', ${invoice.value})" type="button" class="btn btn-success"><i class="bi bi-check-circle"></i></button>
                                    <button style="${showButtons}" onclick="cancelInvoice(${invoice.id})" type="button" class="btn btn-danger"><i class="bi bi-exclamation-octagon"></i></button>
                                </td>
                            </tr>`
                
                totalValue = totalValue + Number(invoice.value);
                totalComission = totalComission + Number(invoice.comission);
                
            });

            listBody+= `<tr>
                            <td colspan="5"></td>
                            <td>Total: $${(totalValue).toFixed(2)}</td>
                            <td>Total: $${(totalComission).toFixed(2)}</td>
                            <td colspan="2"></td>
                        </tr>`

            $('.invoices-table-body').html(
                listBody
            )
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}

listInvoices()

confirmInvoice = (id, email, value) => {
    
        $.ajax({
            method: 'POST',
            url: `http://34.222.146.56:8000/invoices/confirm/`,
            data: JSON.stringify({id: id, method: 'paid', email: email, value: value}),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            success: (res) => {
                listInvoices()
            },
            statusCode: {
                401: () => {
                    console.log('error')
                },
                500: () => {
                    console.log('error')
                }
            }
        })
}

cancelInvoice = (id) => {
    
    $.ajax({
        method: 'POST',
        url: `http://34.222.146.56:8000/invoices/confirm/`,
        data: JSON.stringify({id: id, method: 'cancelled-invoice', email: localStorage.getItem('email')}),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {
            listInvoices()
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}