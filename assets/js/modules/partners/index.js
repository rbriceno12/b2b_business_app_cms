const role = localStorage.getItem('role_id')


const createPartner = () => {

    const email = $('#partner-email').val(),
          name = $('#partner-name').val(),
          createPartnerButton = $('#create-partner-button'),
          img = 'url://testing.com'

    if(!email || !name){
        $('.create-partner-message').html(

            `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                Missing required fields
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`
        )
    }else{
        createPartnerButton.html(
        `<div class="spinner-border text-light" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>`
        )

        createPartnerButton.prop('disabled', true)

        $.ajax({
            method: 'POST',
            url: 'http://34.222.146.56:8000/partners/create-partners/',
            data: JSON.stringify({name: name, email: email, img: img}),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            success: (res) => {

                $('#createPartnerModal').modal('hide')
                listPartners(true)
                createPartnerButton.prop('disabled', false)
                createPartnerButton.html(
                    `Save changes`
                )
                $('.create-partner-message').fadeOut('fast')
                $('#partner-email').val('')
                $('#partner-name').val('')

            },
            statusCode: {
                401: () => {
                    $('.create-partner-message').html(

                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Missing required fields.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                    createPartnerButton.prop('disabled', false)
                    createPartnerButton.html(
                        `Save changes`
                    )
                },
                500: () => {
                    $('.create-partner-message').html(

                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            We have a problem processing your request, try again later.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                    createPartnerButton.prop('disabled', false)
                    createPartnerButton.html(
                        `Save changes`
                    )
                }
            }
        })
    }

    
    
}

const listPartners = (created) => {


    $.ajax({
        method: 'GET',
        url: 'http://34.222.146.56:8000/partners/cms/',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {

            let listBody = ``
            let showButtons
            let greenBackground
            let counter = 0

            role == 1  || role == 2 ? showButtons = '' : showButtons = 'display:none;'
            
            res.data.forEach(partner => {

                counter++

                counter == 1 && created ? greenBackground = 'background-color: #09ab1da6; color: white;' : greenBackground = ''

                listBody+= `<tr>
                                <td style="${greenBackground}">${partner.id}</td>
                                <td style="${greenBackground}"> <img style="width:10%;" src="http://34.222.146.56:8000/public/uploads/${partner.img}" target="_blank"></a></img></td>
                                <td style="${greenBackground}">${partner.name}</td>
                                <td style="${greenBackground}">${partner.email}</td>
                                <td style="${greenBackground}">${partner.status}</td>
                                <td style="${greenBackground}">${partner.created_at}</td>
                                <td style="${greenBackground}">
                                    <button style="${showButtons}" onclick="deletePartner(${partner.id})" type="button" class="btn btn-danger"><i class="bi bi-exclamation-octagon"></i></button>
                                    <button style="${showButtons}" onclick="desactivatePartner(${partner.id})" type="button" class="btn btn-warning"><i class="bi bi-exclamation-triangle"></i></button>
                                </td>
                            </tr>`
                
            });

            $('.partners-table-body').html(
                listBody
            )
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}

listPartners(false)


const deletePartner = (id) => {

    $.ajax({
        method: 'POST',
        url: `http://34.222.146.56:8000/partners/${id}/delete`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {
            listPartners(false)
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })

}

const desactivatePartner = (id) => {

    $.ajax({
        method: 'POST',
        url: `http://34.222.146.56:8000/partners/${id}/desactivated`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {
            listPartners(false)
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })

}

$('#createPartnerForm').submit(function(e) {
    e.preventDefault(); // Prevent default form submission

    const name                = $('#name').val(),
          email                = $('#email').val(),
          file                = $('#file').val(),
          createPartnerButton = $('#create-partner-button')

    if(!name || !email || !file){
        $('.create-partner-message').html(

            `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                Missing required fields
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`
        )
    }else{
        createPartnerButton.html(
            `<div class="spinner-border text-light" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>`
        )

        createPartnerButton.prop('disabled', true)

        const formData = new FormData(this); // Create FormData object

        $.ajax({
            url: 'http://34.222.146.56:8000/partners/upload-partner/', // URL of your Node.js route handler
            type: 'POST',
            data: formData, // Attach FormData object to request
            contentType: false, // Set content type to false for multipart/form-data
            processData: false, // Prevent jQuery from processing data
            success: (data) => {

                $('#createPartnerModal').modal('hide')
                listPartners(true)
                createPartnerButton.prop('disabled', false)
                createPartnerButton.html(
                    `Save changes`
                )
                $('.create-partner-message').fadeOut('fast')
                $('#name').val('')
                $('#email').val('')
                // Handle successful upload response
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.error('Upload failed:', textStatus, errorThrown);
                // Handle upload error
            }
        });
    }

});