const role = localStorage.getItem('role_id')

const listProducts = (created) => {


    $.ajax({
        method: 'GET',
        url: 'http://34.222.146.56:8000/products/cms/',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {

            let listBody = ``,
            showButtons,
            greenBackground,
            counter = 0
            

            role == 1  || role == 2 ? showButtons = '' : showButtons = 'display:none;'
            
            res.data.forEach(product => {


                let productStatus = product.status,
                    showConfirmButton = ''
                
                productStatus == 'activate' ? showConfirmButton = 'display:none;' : showConfirmButton = ''

                counter++

                counter == 1 && created ? greenBackground = 'background-color: #09ab1da6; color: white;' : greenBackground = ''

                listBody+= `<tr>
                                <td style="${greenBackground}">${product.id}</td>
                                <td style="${greenBackground}">${product.partner ?? 'Sin Partner'}</td>
                                <td style="${greenBackground} width: 35%;"> <img style="width:10%;" src="http://34.222.146.56:8000/public/uploads/${product.img}" target="_blank"></a></img></td>
                                <td style="${greenBackground}">${product.name}</td>
                                <td style="${greenBackground}">${product.description}</td>
                                <td style="${greenBackground}">${product.status}</td>
                                <td style="${greenBackground}">${product.created_at}</td>
                                <td style="${greenBackground}">
                                    <button style="${showButtons} ${showConfirmButton}" onclick="updateProductStatus(${product.id}, 'activate')" type="button" class="btn btn-success"><i class="bi bi-check"></i></button>
                                    <button style="${showButtons}" onclick="updateProductStatus(${product.id}, 'desactivate')" type="button" class="btn btn-danger"><i class="bi bi-exclamation-octagon"></i></button>
                                </td>
                            </tr>`
                
            });

            $('.products-table-body').html(
                listBody
            )
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}

listProducts(false)


$('#createProductForm').submit(function(e) {
    e.preventDefault(); // Prevent default form submission

    const name                = $('#name').val(),
          desc                = $('#desc').val(),
          file                = $('#file').val(),
          partnerId           = $('#partner_id').val(),
          createProductButton = $('#create-product-button')

    if(!name || !desc || !file){
        $('.create-product-message').html(

            `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                Missing required fields
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`
        )
    }else{
        createProductButton.html(
            `<div class="spinner-border text-light" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>`
        )

        createProductButton.prop('disabled', true)

        const formData = new FormData(this); // Create FormData object

        $.ajax({
            url: 'http://34.222.146.56:8000/upload-product', // URL of your Node.js route handler
            type: 'POST',
            data: formData, // Attach FormData object to request
            contentType: false, // Set content type to false for multipart/form-data
            processData: false, // Prevent jQuery from processing data
            success: (data) => {

                $('#createProductModal').modal('hide')
                listProducts(true)
                createProductButton.prop('disabled', false)
                createProductButton.html(
                    `Save changes`
                )
                $('.create-product-message').fadeOut('fast')
                $('#desc').val('')
                $('#name').val('')
                $('#file').val('')
                $('#partner_id').val(0)
                // Handle successful upload response
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.error('Upload failed:', textStatus, errorThrown);
                // Handle upload error
            }
        });
    }

});


const loadPartners = () => {
    
    $.ajax({
        method: 'GET',
        url: 'http://34.222.146.56:8000/partners/cms/',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {

            let selectList = `
                            <select class="form-select" name="partner_id" id="partner_id">
                                <option value="0">No asignar a ningun partner</option>`
            res.data.forEach(partner => {

                selectList += `<option value="${partner.id}">${partner.name}</option>`
                
            });
            selectList += `</select>`

            $('.select-partner-div').html(
                selectList
            )
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}


const updateProductStatus = (id, method) => {
    
        $.ajax({
            method: 'PATCH',
            url: `http://34.222.146.56:8000/products/update-status/${method}/${id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            success: (res) => {
                listProducts(false)
            },
            statusCode: {
                401: () => {
                    console.log('error')
                },
                500: () => {
                    console.log('error')
                }
            }
        })
}