const userId = localStorage.getItem('user_id')

const showUser = () => {


    $.ajax({
        method: 'GET',
        url: `http://34.222.146.56:8000/users/${userId}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {

            console.log(res)
            const loggedUserName = $('.logged-user-name')
            loggedUserName.html(
                `${res.data[0].name}`
            )

            const loggedUserRole = $('.logged-user-role')
            loggedUserRole.html(
                `${res.data[0].role}`
            )
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}

showUser()


const sendEmailForgotPassword = () => {

    const forgotPasswordEmail = $('#forgot-password-email').val(),
          forgotPasswordButton = $('#forgot-password-button')

    if(forgotPasswordEmail == '') {
        $('.forgot-password-message').html(
            `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                Debes ingresar tu correo.
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`
        )
    }else{
        forgotPasswordButton.html(
            `<div class="spinner-border text-light" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>`
        )

        $.ajax({
            method: 'POST',
            url: `http://34.222.146.56:8000/auth/forgot-password/`,
            data: JSON.stringify({email: forgotPasswordEmail}),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            success: (res) => {

                forgotPasswordButton.html(
                    `Cambiar Password`
                )

                $('.forgot-password-message').html(
                    `<div class="alert alert-success alert-dismissible fade show" role="alert">
                        Hemos enviado un Link de recuperacion a tu bandeja de correo.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`
                )
                
            },
            statusCode: {
                401: () => {
                    console.log('error')
                    $('.forgot-password-message').html(
                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Hubo un problema, revisa que tu correo sea valido o intenta mas tarde.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                },
                500: () => {
                    console.log('error')
                    $('.forgot-password-message').html(
                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Hubo un problema, revisa que tu correo sea valido o intenta mas tarde.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                }
            }
        })
    }

}