const role = localStorage.getItem('role_id')

const listPurchaseOrders = (created) => {


    $.ajax({
        method: 'GET',
        url: 'http://34.222.146.56:8000/purchase-orders/list/',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {

            console.log(res)

            let listBody = ``
            let showButtons
            let greenBackground
            let counter = 0
            const countResponse = res.data.length
            
            if(countResponse == 0){
                listBody+= `<tr>
                                <td colspan="6" style="text-align:center">No hay ordenes de compra</td>
                            </tr>`
            }else{
                role == 1  || role == 2 ? showButtons = '' : showButtons = 'display:none;'
            
                res.data.forEach(purchaseOrder => {

                    counter++

                    counter == 1 && created ? greenBackground = 'background-color: #09ab1da6; color: white;' : greenBackground = ''
                    
                    let purchaseOrderStatus = purchaseOrder.status

                    switch (purchaseOrderStatus) {
                        case 'active':
                            purchaseOrderStatus = 'Activa'
                            break;
                        case 'cancelled-purchase-order':
                            purchaseOrderStatus = 'Cancelada'
                            break;
                        default:
                            break;
                    }

                    listBody+= `<tr>
                                    <td style="${greenBackground}">${purchaseOrder.id}</td>
                                    <td style="${greenBackground}">${purchaseOrder.email}</td>
                                    <td style="${greenBackground}width:20%"> <a target="_blank" href="http://34.222.146.56:8000/public/uploads/${purchaseOrder.file}">Ver archivo</a></td>
                                    <td style="${greenBackground}">${purchaseOrderStatus}</td>
                                    <td style="${greenBackground}">${purchaseOrder.created_at}</td>
                                    <td style="${greenBackground}">
                                        <button style="${showButtons}" onclick="openConfirmOrderModal(${purchaseOrder.id}, ${purchaseOrder.user_id}, '${purchaseOrder.email}')" type="button" class="btn btn-success"><i class="bi bi-check-circle"></i></button>
                                        <button style="${showButtons}" onclick="deletePurchaseOrder(${purchaseOrder.id})" type="button" class="btn btn-danger"><i class="bi bi-exclamation-octagon"></i></button>
                                    </td>
                                </tr>`
                    
                });
            }

            $('.purchase-orders-table-body').html(
                listBody
            )
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}

listPurchaseOrders(false)

const openConfirmOrderModal = (id, userId, email) => {
    $('#confirmPurchaseOrderModal').modal('show')
    $('#confirm-order-id').attr('data-id', id)
    $('#confirm-order-user-id').attr('data-id', userId)
    $('#confirm-order-user-email').attr('data-id', email)

}

const confirmPurchaseOrder = () => {

    const id = $('#confirm-order-id').attr('data-id'),
          userId = $('#confirm-order-user-id').attr('data-id'),
          email = $('#confirm-order-user-email').attr('data-id'),
          value = $('#value').val(),
          updatePurchaseOrderButton = $('#confirm-purchase-order-button')

    if(!value){
        $('.confirm-purchase-order-message').html(
        
            `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                Debes ingresar un valor de la factura para poder confirmar
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`
        )
    }else{

        updatePurchaseOrderButton.html(
            `<div class="spinner-border text-light" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>`
        )
        updatePurchaseOrderButton.prop('disabled', true)

        $.ajax({
            method: 'POST',
            url: `http://34.222.146.56:8000/purchase-orders/${id}/invoice`,
            data: JSON.stringify({ value: value, user: userId, email: email}),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            success: (res) => {
                
                updatePurchaseOrderButton.html(
                    `<div class="spinner-border text-light" role="status">
                        <span class="visually-hidden">Save changes</span>
                    </div>`
                )
                updatePurchaseOrderButton.prop('disabled', false)

                $('#confirmPurchaseOrderModal').modal('hide')

                listPurchaseOrders()
            },
            statusCode: {
                401: () => {
                    console.log('error')
                },
                500: () => {
                    console.log('error')
                }
            }
        })
    }

    
}

const deletePurchaseOrder = (id) => {
    $.ajax({
        method: 'POST',
        url: `http://34.222.146.56:8000/purchase-orders/${id}/cancelled-purchase-order`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {
            console.log(res)
            listPurchaseOrders()
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}

const desactivatePurchaseOrder = (id) => {
    $.ajax({
        method: 'POST',
        url: `http://34.222.146.56:8000/purchase-orders/${id}/desactivate`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {
            console.log(res)
            listPurchaseOrders()
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}


const selectClient = (id, name, email) => {
    $(".search-results").fadeOut('fast');
    $('#selected-client').val(id)
    $('#search-clients-box').val(`${name} - ${email}`)
    $("#client-search-purchase-orders").html("");
}

$(document).ready(function() {
    $("#search-clients-box").keyup(function() {
        const searchText = $(this).val().trim();

        if (searchText.length >= 3) { // Minimum search length (optional)
          $.ajax({
            url: "http://34.222.146.56:8000/clients/search/", // Replace with your search endpoint URL
            type: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            data: JSON.stringify({ q: searchText }), // Send search query as data
            success: (data) => {

              $(".search-results").fadeIn('fast');
              let clientOptions = ``
              data.data.forEach(element => {
                clientOptions+= `<p><a href="javascript:;" onclick="selectClient(${element.id}, '${element.name}', '${element.email}')" id="${element.id}">${element.name} - ${element.email}</a></p>`
              });

              $("#client-search-purchase-orders").html(clientOptions); // Update results container

            },
            error: (jqXHR, textStatus, errorThrown) => {
              console.error("Error! Search failed:", textStatus, errorThrown);
              $(".search-results").fadeOut('fast');
              $("#client-search-purchase-orders").html("Error occurred during search.");
            }
          });
        } else {
          $('#selected-client').val(null)
          $("#client-search-purchase-orders").html(""); // Clear results if search term is too short
        }
    });

    $('#isNewClient').on('change', function () {
        
        if($(this).prop('checked')){
            $('.old-client').fadeOut('fast')
            $('.new-client').fadeIn('fast')
        }else{
            $('.old-client').fadeIn('fast')
            $('.new-client').fadeOut('fast')
        }

    })

    $('#createPurchaseOrderForm').submit(function(e) {
        e.preventDefault(); // Prevent default form submission

        let success = false

        const file = $('#file').val(),
        createPurchaseOrderButton = $('#create-purchase-order-button')

        let formData

        if(!file){
            $('.create-purchase-order-message').html(
        
                `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    Upload a file
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>`
            )
        }else{
            if($('#isNewClient').prop('checked')){
                const name = $('#client-name').val(),
                  email = $('#client-email').val()
    
                if(!name || !email){
                    $('.create-purchase-order-message').html(
            
                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Missing required fields
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
    
                }else{
                    createPurchaseOrderButton.html(
                        `<div class="spinner-border text-light" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>`
                    )
            
                    createPurchaseOrderButton.prop('disabled', true)

                    success = true
                    formData = new FormData(this); // Create FormData object
                }
    
                
            }else{
                const clientId = $('#selected-client').val()

                if(!clientId){
                    $('.create-purchase-order-message').html(
            
                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Select a Client
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
    
                }else{
                    success = true
                    formData = new FormData(this);
                    formData.append('user_client_id', clientId)
                }

            }

            if(success == true){
                $.ajax({
                    url: 'http://34.222.146.56:8000/purchase-orders/', // URL of your Node.js route handler
                    type: 'POST',
                    data: formData, // Attach FormData object to request
                    contentType: false, // Set content type to false for multipart/form-data
                    processData: false, // Prevent jQuery from processing data
                    success: (data) => {
                        $('#createPurchaseOrderModal').modal('hide')
                        listPurchaseOrders(true)
                        createPurchaseOrderButton.prop('disabled', false)
                        createPurchaseOrderButton.html(
                            `Save changes`
                        )
                        $('.create-purchase-order-message').fadeOut('fast')
                        $('#desc').val('')
                        $('#name').val('')
                        $('#file').val('')
                        // Handle successful upload response
                    },
                    error: (jqXHR, textStatus, errorThrown) => {
                        console.error('Upload failed:', textStatus, errorThrown);
                        // Handle upload error
                    }
                });
            }
        }
    
    });
});