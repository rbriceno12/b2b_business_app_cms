const listSidebar = (created) => {


    $.ajax({
        method: 'GET',
        url: 'http://34.222.146.56:8000/sidebar/list/',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {
            console.log(res)
            let listSidebar = ``
            // let showButtons
            // let greenBackground
            // let counter = 0

            // role == 1  || role == 2 ? showButtons = '' : showButtons = 'display:none;'

            res.data.forEach(option => {

            //     counter++

            //     counter == 1 && created ? greenBackground = 'background-color: #09ab1da6; color: white;' : greenBackground = ''
                
                listSidebar+= ` <!-- ======= Sidebar ======= -->
                                <li class="nav-item">
                                    <a class="nav-link " href="${option.url}">
                                    ${option.icon}
                                    <span>${option.name}</span>
                                    </a>
                                </li>`
            });

            $('#sidebar-nav').html(
                listSidebar
            )
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}

listSidebar(false)

const deleteUser = (id) => {

    $.ajax({
        method: 'POST',
        url: `http://34.222.146.56:8000/users/${id}/delete`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {
            listUsers(false)
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })

}

const desactivateUser = (id) => {

    $.ajax({
        method: 'POST',
        url: `http://34.222.146.56:8000/users/${id}/desactivate`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {
            listUsers(false)
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })

}