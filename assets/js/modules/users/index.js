
const role = localStorage.getItem('role_id')

const createUser = () => {

    const email = $('#user-email').val(),
          name = $('#user-name').val(),
          role = $('#user-type').val(),
          password = $('#user-password').val(),
          createUserButton = $('#create-user-button')

    if(!email || !name){
        $('.create-user-message').html(

            `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                Missing required fields
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`
        )
    }else{
        createUserButton.html(
        `<div class="spinner-border text-light" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>`
        )

        createUserButton.prop('disabled', true)

        $.ajax({
            method: 'POST',
            url: 'http://34.222.146.56:8000/users/create-users/',
            data: JSON.stringify({name: name, email: email, password: password, role: role}),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            success: (res) => {

                $('#createUserModal').modal('hide')
                listUsers(true)
                createUserButton.prop('disabled', false)
                createUserButton.html(
                    `Save changes`
                )
                $('#user-email').val('')
                $('#user-name').val('')

            },
            statusCode: {
                401: () => {
                    $('.create-user-message').html(

                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            User already exists.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                    createUserButton.prop('disabled', false)
                    createUserButton.html(
                        `Save changes`
                    )
                },
                500: () => {
                    $('.create-user-message').html(

                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            We have a problem processing your request, try again later.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                    createUserButton.prop('disabled', false)
                    createUserButton.html(
                        `Save changes`
                    )
                }
            }
        })
    }

    
    
}

const listUsers = (created) => {


    $.ajax({
        method: 'GET',
        url: 'http://34.222.146.56:8000/users/list/',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {

            let listBody = ``
            let showButtons
            let greenBackground
            let counter = 0

            role == 1  || role == 2 ? showButtons = '' : showButtons = 'display:none;'

            res.data.forEach(user => {

                counter++

                counter == 1 && created ? greenBackground = 'background-color: #09ab1da6; color: white;' : greenBackground = ''
                
                listBody+= `<tr>
                                <td style="${greenBackground}">${user.id}</td>
                                <td style="${greenBackground}">${user.name}</td>
                                <td style="${greenBackground}">${user.email}</td>
                                <td style="${greenBackground}">${user.status}</td>
                                <td style="${greenBackground}">${user.role}</td>
                                <td style="${greenBackground}">${user.created_at}</td>
                                <td style="${greenBackground}">
                                    <button style="${showButtons}" onclick="deleteUser(${user.id})" type="button" class="btn btn-danger"><i class="bi bi-exclamation-octagon"></i></button>
                                    <button style="${showButtons}" onclick="desactivateUser(${user.id})" type="button" class="btn btn-warning"><i class="bi bi-exclamation-triangle"></i></button>
                                </td>
                            </tr>`
                
            });

            $('.users-table-body').html(
                listBody
            )
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })
}

listUsers(false)

const deleteUser = (id) => {

    $.ajax({
        method: 'POST',
        url: `http://34.222.146.56:8000/users/${id}/delete`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {
            listUsers(false)
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })

}

const desactivateUser = (id) => {

    $.ajax({
        method: 'POST',
        url: `http://34.222.146.56:8000/users/${id}/desactivate`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        success: (res) => {
            listUsers(false)
            
        },
        statusCode: {
            401: () => {
                console.log('error')
            },
            500: () => {
                console.log('error')
            }
        }
    })

}