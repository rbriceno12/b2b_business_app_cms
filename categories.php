<!DOCTYPE html>
<html lang="en">

<title>Categories</title>
<?php include 'views/head.php' ?>

<body>

  
  <?php include 'views/header.php'; ?>
  <?php include 'views/sidebar.php'; ?>

  <!-- Modal -->
  <div class="modal fade" id="createCategoryModal" tabindex="-1" aria-labelledby="createCategoryModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="createCategoryModalLabel">Crear Categoria</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="create-category-message"></div>
          <form id="createCategoryForm">
            <div class="row mb-3">
                <label for="inputFile" class="col-sm-2 col-form-label">File Upload</label>
                <div class="col-sm-10">
                    <input class="form-control" type="file" id="file" name="file">
                </div>
            </div>
            <div class="row mb-3">
              <label for="inputName" class="col-sm-2 col-form-label">Name</label>
              <div class="col-sm-10">
                <input id="name" name="name" type="text" class="form-control">
              </div>
            </div>
            <button id="create-category-button" type="submit" class="btn btn-primary">Guardar</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

  <main id="main" class="main">

    <div class="pagetitle">
      <h1>Categories Table</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active"><a href="categories.php">Categories</a></li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <!-- <h5 class="card-title">Users List</h5> -->
              <button type="button" class="btn btn-primary rounded-pill create-buttons" data-bs-toggle="modal" data-bs-target="#createCategoryModal">Crear Categoria</button>

              <!-- Default Table -->
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Img</th>
                    <th scope="col">Name</th>
                    <th scope="col">Status</th>
                    <th scope="col">Start Date</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody class="categories-table-body">
                  
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>

        
      </div>
    </section>

  </main><!-- End #main -->
  
  <?php include 'views/footer.php'; ?>


  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <?php include 'views/scripts.php'; ?>
  <script src="assets/js/modules/categories/index.js"></script>
  <script src="assets/js/modules/auth/validate.js"></script>

</body>

</html>