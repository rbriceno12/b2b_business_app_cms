<!DOCTYPE html>
<html lang="en">

<title>Invoices</title>
<?php include 'views/head.php' ?>

<body>

  
  <?php include 'views/header.php'; ?>
  <?php include 'views/sidebar.php'; ?>

  <main id="main" class="main">

    <div class="pagetitle">
      <h1>Invoices</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active"><a href="invoices.php">Invoices</a></li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Invoices</h5>

              <!-- Default Table -->
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Client</th>
                    <th scope="col">File</th>
                    <th scope="col">Status</th>
                    <th scope="col">Start Date</th>
                    <th scope="col">Value</th>
                    <th scope="col">Comision</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody class="invoices-table-body">
                  
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>

        
      </div>
    </section>

  </main><!-- End #main -->
  
  <?php include 'views/footer.php'; ?>


  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <?php include 'views/scripts.php'; ?>
  <script src="assets/js/modules/invoices/index.js"></script>
  <script src="assets/js/modules/auth/validate.js"></script>

</body>

</html>