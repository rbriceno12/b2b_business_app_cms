<!DOCTYPE html>
<html lang="en">

<title>Forgot Password Page</title>

<?php include 'views/head.php'; ?>

<body>

  <main>
    <div class="container">

      <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

              <div class="d-flex justify-content-center py-4">
                <a href="index.html" class="logo d-flex align-items-center w-auto">
                  <img src="assets/img/logo.png" alt="">
                  <span class="d-none d-lg-block">TKotizo</span>
                </a>
              </div><!-- End Logo -->

              <div class="card mb-3">

                <div class="card-body">
                <!-- <div class="alert alert-success alert-dismissible fade show" role="alert">
                A simple success alert—check it out!
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>

              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                A simple danger alert—check it out!
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div> -->
                  <div class="pt-4 pb-2">
                    <div class="forgot-password-message"></div>
                  </div>
                  <div class="pt-4 pb-2">
                    <h5 class="card-title text-center pb-0 fs-4">Change your password</h5>
                    <p class="text-center small">Enter your email & new password</p>
                  </div>

                  <form class="row g-3 needs-validation" novalidate>

                    <div class="col-12">
                      <label for="yourUsername" class="form-label">Email</label>
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend">@</span>
                        <input type="email" name="forgot-password-email" class="form-control" id="forgot-password-email" required>
                        <div class="invalid-feedback">Please enter your email.</div>
                      </div>
                    </div>

                    <div class="col-12">
                      <label for="yourPassword" class="form-label">New Password</label>
                      <input type="password" name="forgot-password-password" class="form-control" id="forgot-password-password" required>
                      <div class="invalid-feedback">Please enter your password!</div>
                    </div>

                    <div class="col-12">
                      <button id="forgot-password-button" class="btn btn-primary w-100" type="button" onclick="changePassword()">Cambiar Password</button>
                    </div>
                    <div class="col-12">
                      <p class="small mb-0">Already have an account? <a href="pages-login.php">Login</a></p>
                    </div>
                    <div class="col-12">
                      <p class="small mb-0">Don't have account? <a href="pages-register.php">Create an account</a></p>
                    </div>
                  </form>

                </div>
              </div>

              <?php include 'views/footer.php'; ?>

            </div>
          </div>
        </div>

      </section>

    </div>
  </main><!-- End #main -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <?php include 'views/scripts.php'; ?>
  <script>
    const toggleForgotYourPassword = () => {
      $('.forgot-your-password-input').toggle()
    }


    const changePassword = () => {
      const forgotPasswordEmail = $('#forgot-password-email').val(),
          forgotPasswordPassword = $('#forgot-password-password').val(),
          forgotPasswordButton = $('#forgot-password-button')

    if(forgotPasswordEmail == '' || forgotPasswordPassword == '') {
        $('.forgot-password-message').html(
            `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                Debes ingresar tu correo y un password.
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`
        )
    }else{
        forgotPasswordButton.html(
            `<div class="spinner-border text-light" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>`
        )

        $.ajax({
            method: 'POST',
            url: `http://34.222.146.56:8000/auth/change-password/`,
            data: JSON.stringify({email: forgotPasswordEmail, password: forgotPasswordPassword}),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            success: (res) => {

                forgotPasswordButton.html(
                    `Cambiar Password`
                )

                $('.forgot-password-message').html(
                    `<div class="alert alert-success alert-dismissible fade show" role="alert">
                        Tu password ha sido modificado exitosamente. <a href="pages-login.php">Inicia sesion</a>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`
                )
                
            },
            statusCode: {
                401: () => {
                    console.log('error')
                    $('.forgot-password-message').html(
                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Hubo un problema, revisa que tu correo sea valido o intenta mas tarde.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                },
                500: () => {
                    console.log('error')
                    $('.forgot-password-message').html(
                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Hubo un problema, revisa que tu correo sea valido o intenta mas tarde.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                }
            }
        })
    }
    }

  </script>

</body>

</html>