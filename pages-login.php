<!DOCTYPE html>
<html lang="en">

<title>Login Page</title>

<?php include 'views/head.php'; ?>

<body>

  <main>
    <div class="container">

      <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

              <div class="d-flex justify-content-center py-4">
                <a href="index.html" class="logo d-flex align-items-center w-auto">
                  <img src="assets/img/logo.png" alt="">
                  <span class="d-none d-lg-block">TKotizo</span>
                </a>
              </div><!-- End Logo -->

              <div class="card mb-3">

                <div class="card-body">
                <!-- <div class="alert alert-success alert-dismissible fade show" role="alert">
                A simple success alert—check it out!
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>

              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                A simple danger alert—check it out!
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div> -->
                  <div class="pt-4 pb-2">
                    <div class="auth-message"></div>
                  </div>
                  <div class="pt-4 pb-2">
                    <h5 class="card-title text-center pb-0 fs-4">Login to Your Account</h5>
                    <p class="text-center small">Enter your username & password to login</p>
                  </div>

                  <form class="row g-3 needs-validation" novalidate>

                    <div class="col-12">
                      <label for="yourUsername" class="form-label">Email</label>
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend">@</span>
                        <input type="email" name="email" class="form-control" id="email" required>
                        <div class="invalid-feedback">Please enter your email.</div>
                      </div>
                    </div>

                    <div class="col-12">
                      <label for="yourPassword" class="form-label">Password</label>
                      <input type="password" name="password" class="form-control" id="password" required>
                      <div class="invalid-feedback">Please enter your password!</div>
                    </div>

                    <div class="col-12">
                      <button id="login-button" class="btn btn-primary w-100" type="button" onclick="login()">Login</button>
                    </div>
                    <div class="col-12">
                      <p class="small mb-0">Don't have account? <a href="pages-register.php">Create an account</a></p>
                    </div>
                    <div class="col-12">
                      <p class="small mb-0">Forgot your password? <a href="javascript:toggleForgotYourPassword();">Restore my password</a></p>
                    </div>
                    <div class="col-12 forgot-your-password-input" style="display: none;">
                      <label for="yourUsername" class="form-label">Email</label>
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend">@</span>
                        <input type="email" name="forgot-password-email" class="form-control" id="forgot-password-email" required>
                        <div class="invalid-feedback">Please enter your email.</div>
                      </div><br>
                      <button id="forgot-password-button" class="btn btn-primary w-100" type="button" onclick="sendEmailForgotPassword()">Cambiar Password</button>
                    </div>
                  </form>

                </div>
              </div>

              <?php include 'views/footer.php'; ?>

            </div>
          </div>
        </div>

      </section>

    </div>
  </main><!-- End #main -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <?php include 'views/scripts.php'; ?>
  <script>
    const toggleForgotYourPassword = () => {
      $('.forgot-your-password-input').toggle()
    }


    const sendEmailForgotPassword = () => {
      const forgotPasswordEmail = $('#forgot-password-email').val(),
          forgotPasswordButton = $('#forgot-password-button')

    if(forgotPasswordEmail == '') {
        $('.auth-message').html(
            `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                Debes ingresar tu correo.
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`
        )
    }else{
        forgotPasswordButton.html(
            `<div class="spinner-border text-light" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>`
        )

        $.ajax({
            method: 'POST',
            url: `http://34.222.146.56:8000/auth/forgot-password/`,
            data: JSON.stringify({email: forgotPasswordEmail}),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            success: (res) => {

                forgotPasswordButton.html(
                    `Cambiar Password`
                )

                $('.auth-message').html(
                    `<div class="alert alert-success alert-dismissible fade show" role="alert">
                        Hemos enviado un Link de recuperacion a tu bandeja de correo.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`
                )
                
            },
            statusCode: {
                401: () => {
                    console.log('error')
                    $('.auth-message').html(
                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Hubo un problema, revisa que tu correo sea valido o intenta mas tarde.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                },
                500: () => {
                    console.log('error')
                    $('.auth-message').html(
                        `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Hubo un problema, revisa que tu correo sea valido o intenta mas tarde.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>`
                    )
                }
            }
        })
    }
    }

  </script>

</body>

</html>