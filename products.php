<!DOCTYPE html>
<html lang="en">

<title>Products</title>
<?php include 'views/head.php' ?>

<body>

  
  <?php include 'views/header.php'; ?>
  <?php include 'views/sidebar.php'; ?>

  <!-- Modal -->
  <div class="modal fade" id="createProductModal" tabindex="-1" aria-labelledby="createProductModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="createProductModalLabel">Crear Producto</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="create-product-message"></div>
          <form id="createProductForm">
            <div class="row mb-3">
                <label for="inputFile" class="col-sm-2 col-form-label">File Upload</label>
                <div class="col-sm-10">
                    <input class="form-control" type="file" id="file" name="file">
                </div>
            </div>
            <div class="row mb-3">
              <label for="selectPartner" class="col-sm-2 col-form-label">Partner</label>
              <div class="col-sm-10 select-partner-div">
                
              </div>
            </div>
            <div class="row mb-3">
              <label for="inputName" class="col-sm-2 col-form-label">Name</label>
              <div class="col-sm-10">
                <input id="name" name="name" type="text" class="form-control">
              </div>
            </div>
            <div class="row mb-3">
              <label for="inputEmail" class="col-sm-2 col-form-label">Description</label>
              <div class="col-sm-10">
                <input id="desc" name="desc" type="text" class="form-control">
              </div>
            </div>
            <button id="create-product-button" type="submit" class="btn btn-primary">Guardar</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
          <!-- <button id="create-user-button" onclick="createProduct()" type="button" class="btn btn-primary">Save changes</button> -->
        </div>
      </div>
    </div>
  </div>

  <main id="main" class="main">

    <div class="pagetitle">
      <h1>Products Table</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active"><a href="products.php">Products</a></li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <button type="button" class="btn btn-primary rounded-pill create-buttons" data-bs-toggle="modal" data-bs-target="#createProductModal" onclick="loadPartners()">Create Product</button>

              <!-- Default Table -->
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Partner</th>
                    <th scope="col">Img</th>
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Status</th>
                    <th scope="col">Start Date</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody class="products-table-body">
                  
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>

        
      </div>
    </section>

  </main><!-- End #main -->
  
  <?php include 'views/footer.php'; ?>


  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <?php include 'views/scripts.php'; ?>
  <script src="assets/js/modules/products/index.js"></script>
  <script src="assets/js/modules/auth/validate.js"></script>

</body>

</html>