<!DOCTYPE html>
<html lang="en">

<title>Purchase Orders</title>
<?php include 'views/head.php' ?>

<body>

  
  <?php include 'views/header.php'; ?>
  <?php include 'views/sidebar.php'; ?>
  <input id="selected-client" type="hidden" value="">
  <input id="confirm-order-id" type="hidden" value="">
  <input id="confirm-order-user-id" type="hidden" value="">
  <input id="confirm-order-user-email" type="hidden" value="">
  

  <!-- Modal -->
  <div class="modal fade" id="createPurchaseOrderModal" tabindex="-1" aria-labelledby="createPurchaseOrderModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="createPurchaseOrderModalLabel">Create PurchaseOrder</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="create-purchase-order-message"></div>
          <div class="container">
            <form id="createPurchaseOrderForm">
                <div class="row mb-3">
                    <label for="inputNumber" class="col-sm-2 col-form-label">File Upload</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="file" id="file" name="file">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="form-check form-switch">
                        <input class="form-check-input" type="checkbox" id="isNewClient">
                        <label class="form-check-label" for="isNewClient">New Client</label>
                    </div>
                </div>
                <div class="row mb-3 old-client">
                    <input class="form-control" type="text" id="search-clients-box" placeholder="Enter Client name or email at least 3 characters">
                    <div class="search-results" id="client-search-purchase-orders"></div>
                </div>
                <div class="row mb-3 new-client">
                    <input class="form-control" type="text" id="client-name" name="name" placeholder="Enter client name">
                </div>
                <div class="row mb-3 new-client">
                    <input class="form-control" type="text" id="client-email" name="email" placeholder="Enter client email">
                </div>
                
                <!-- <select class="form-select" multiple aria-label="multiple select example">
                    <option selected>Open this select menu</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select> -->
                <button id="create-purchase-order-button" type="submit" class="btn btn-primary">Save changes</button>
            </form>
          </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
          <!-- <button id="create-user-button" onclick="createPurchaseOrder()" type="button" class="btn btn-primary">Save changes</button> -->
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="confirmPurchaseOrderModal" tabindex="-1" aria-labelledby="confirmPurchaseOrderModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="confirmPurchaseOrderModalLabel">Create PurchaseOrder</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="confirm-purchase-order-message"></div>
          <div class="container">
            <form id="createPurchaseOrderForm">
                <div class="row mb-3 old-client">
                    <label for="inputNumber" class="col-form-label">Valor de la factura</label>
                    <input class="form-control" type="number" id="value" placeholder="Ingresa el valor de la factura">
                    <div class="search-results" id="client-search-purchase-orders"></div>
                </div>
                <button id="confirm-purchase-order-button" onclick="confirmPurchaseOrder()" type="button" class="btn btn-primary">Save changes</button>
            </form>
          </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
          <!-- <button id="create-user-button" onclick="createPurchaseOrder()" type="button" class="btn btn-primary">Save changes</button> -->
        </div>
      </div>
    </div>
  </div>

  <main id="main" class="main">

    <div class="pagetitle">
      <h1>Purchase Orders</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active"><a href="purchase-orders.php">Purchase Orders</a></li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
            <button type="button" class="btn btn-primary rounded-pill create-buttons" data-bs-toggle="modal" data-bs-target="#createPurchaseOrderModal">Create Purchase Order</button>
              <!-- <h5 class="card-title">Purchase Orders</h5> -->
              

              <!-- Default Table -->
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Client</th>
                    <th scope="col">File</th>
                    <th scope="col">Status</th>
                    <th scope="col">Start Date</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody class="purchase-orders-table-body">
                  
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>

        
      </div>
    </section>

  </main><!-- End #main -->
  
  <?php include 'views/footer.php'; ?>


  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <?php include 'views/scripts.php'; ?>
  <script src="assets/js/modules/purchase-orders/index.js"></script>
  <script src="assets/js/modules/auth/validate.js"></script>

</body>

</html>