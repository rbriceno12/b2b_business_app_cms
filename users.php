<!DOCTYPE html>
<html lang="en">

<title>Users</title>
<?php include 'views/head.php' ?>

<body>

  
  <?php include 'views/header.php'; ?>
  <?php include 'views/sidebar.php'; ?>

  <!-- Modal -->
  <div class="modal fade" id="createUserModal" tabindex="-1" aria-labelledby="createUserModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="createUserModalLabel">Create User</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="create-user-message"></div>
          <form>
            <div class="row mb-3">
              <label for="inputName" class="col-sm-2 col-form-label">Nombre</label>
              <div class="col-sm-10">
                <input id="user-name" type="text" class="form-control">
              </div>
            </div>
            <div class="row mb-3">
              <label for="inputEmail" class="col-sm-2 col-form-label">Rol</label>
              <div class="col-sm-10 user-type-select">
                <!-- <input id="user-email" type="email" class="form-control"> -->
                <select name="user-type" id="user-type" class="form-control">
                  <option value="1">ADMIN</option>
                  <option value="2">AGENT</option>
                  <option value="3">ACCOUNTANT</option>
                  <option value="4">CLIENT</option>
                </select>
              </div>
            </div>
            <div class="row mb-3">
              <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
              <div class="col-sm-10">
                <input id="user-email" type="email" class="form-control">
              </div>
            </div>
            <div class="row mb-3">
              <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
              <div class="col-sm-10">
                <input id="user-password" type="password" class="form-control">
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
          <button id="create-user-button" onclick="createUser()" type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>

  <main id="main" class="main">

    <div class="pagetitle">
      <h1>Users Table</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active"><a href="users.php">Users</a></li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <!-- <h5 class="card-title">Users List</h5> -->
              <button type="button" class="btn btn-primary rounded-pill create-buttons" data-bs-toggle="modal" data-bs-target="#createUserModal">Create User</button>

              <!-- Default Table -->
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Status</th>
                    <th scope="col">Role</th>
                    <th scope="col">Start Date</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody class="users-table-body">
                  
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>

        
      </div>
    </section>

  </main><!-- End #main -->
  
  <?php include 'views/footer.php'; ?>


  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <?php include 'views/scripts.php'; ?>
  <script src="assets/js/modules/users/index.js"></script>
  <script src="assets/js/modules/auth/validate.js"></script>

</body>

</html>